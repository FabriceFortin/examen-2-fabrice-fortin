﻿using Microsoft.AspNetCore.Mvc;
using ScoreBoard.Models;

namespace ScoreBoard.Controllers
{
    public class JoueurController : Controller
    {
        private readonly IJoueurRepository _ListeJoueurs;
        public JoueurController(IJoueurRepository _listeJoueurs)
        {
            this._ListeJoueurs = _listeJoueurs;
        }
        public IActionResult Index()
        {
            return View(_ListeJoueurs.ListeJoueurs);
        }
        [HttpGet]
        public IActionResult Modifier(int id)
        {
            
            return View(_ListeJoueurs.GetJoueur(id));
        }
        [HttpPost]
        public IActionResult Modifier(Joueur joueur)
        {
            if (ModelState.IsValid)
            {
                _ListeJoueurs.Modifier(joueur);
                return View("Index",_ListeJoueurs.ListeJoueurs);
            }
            else { 
                return View("Modifier", joueur);
            }
        }
        [HttpGet]
        public IActionResult Supprimer(int id)
        {

            return View(_ListeJoueurs.GetJoueur(id));
        }
        [HttpPost]
        public IActionResult Supprimer(Joueur joueur)
        {

            _ListeJoueurs.Supprimer(joueur.Id);
            return View("Index", _ListeJoueurs.ListeJoueurs);
        }
    }
}
