﻿namespace ScoreBoard.Models
{
    public interface IJoueurRepository
    {
        public List<Joueur> ListeJoueurs { get; set; }
        public Joueur? GetJoueur(int id);
        public void Ajouter(Joueur joueur);
        public void Modifier(Joueur joueur);
        public void Supprimer(int id);
    }
}
