﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ScoreBoard.Models
{
    public class Joueur
    {
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DisplayName("Id")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Le Nom doit avoir entre 2 et 20 caracteres")]
        [DisplayName("Nom")]
        public string Nom { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Le Prenom doit avoir entre 2 et 20 caracteres")]
        [DisplayName("Prenom")]
        public string Prenom { get; set; }
        [DisplayName("Equipe")]
        //[RegularExpression(@"^[A-Z]{2,4}$ || ()",ErrorMessage = "L'équipe doit etre une chaine de charactères de 2 à 4 lettres majuscules ou vide")]
        public string? Equipe { get; set; }
        [DisplayName("Telephone")]
        public string? Telephone { get; set; }
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Désolé, l'adresse email est invalide.")]
        [DisplayName("Courriel")]
        public string Courriel { get; set; }
        [DisplayName("Jeux")]
        public List<Jeu>? Jeux { get; set; }
    }
}
